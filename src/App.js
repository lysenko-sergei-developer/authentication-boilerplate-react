import React, { Component } from 'react'

import Authentication from './components/Authentication'
import Home from './components/Home'

import './App.css'

class App extends Component {
  constructor () {
    super()
  
    this.state = {
      authenticated: false
    }
  }

  componentDidMount() {
    if (localStorage.token != null) 
      this.setState({ authenticated: true })
  }

  handleAuthenticated = () => this.setState ({ authenticated: !this.state.authenticated })

  handleGetToken = (newToken) => this.setState ({ token : newToken})
  
  handleRemoveToken = () => {
    this.setState ({
      token: '',
    })

    localStorage.removeItem('token')
    this.handleAuthenticated()
  }
  
  render() {
    return (
      <div className="App">
        {this.state.authenticated ? 
          <Home handleAuthenticated={this.handleAuthenticated}
                handleRemoveToken={this.handleRemoveToken}
                token={this.state.token}/> : 
          <Authentication handleAuthenticated={this.handleAuthenticated}
                          handleGetToken={this.handleGetToken}/>
        }
      </div>
    );
  }
}

export default App;
