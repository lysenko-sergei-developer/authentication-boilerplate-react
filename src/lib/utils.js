import bcrypt from 'bcrypt-nodejs'

export const bcryptPassword  = (password) => bcrypt.hashSync(password) 

export const validateField = (field) => {
  field = field.trim()

  const pattern = new RegExp(/\w/)
  const validate = pattern.test(field)
  
  return (field.length >= 6 && validate)
}

export const generateId = () => Math.floor(Math.random()*1000000)