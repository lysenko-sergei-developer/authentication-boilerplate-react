import React, { Component } from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'

import { bcryptPassword, validateField } from '../lib/utils'

import { Form } from './form/Form'

import './Authentication.css'

export default class Authentication extends Component {
  constructor (props) {
    super(props)

    this.state = {
      username: '',
      password: '',
      login: true,
      formError: '',
    }
  }

  createAuthentication = (token) => {
      localStorage.setItem('token', token)
      this.props.handleGetToken(token)
      this.props.handleAuthenticated()
  }

  receiveToken = () => {
    const url = 'https://grape-auth-boilerplate.herokuapp.com'
    const username = this.state.username
    const password = this.state.password

     axios.get(`${url}/users/token?username=${username}&password=${password}`)
      .then(response => { this.createAuthentication(response.data) } )
      .catch(req => this.setState ({ formError: req.response.data.errors[0] }) )
  }

  createUser = () => {
    const url = 'https://grape-auth-boilerplate.herokuapp.com' // change to your API

    const userData = {
        username: this.state.username,
        hashed_password: bcryptPassword(this.state.password)
    }

    axios.post(`${url}/users`, userData)
    .then(response => response.status === 201 && this.receiveToken())
    .catch(req => this.setState ({ formError: req.response.data.errors[0] }) )
  }

  handleCreateUserRequest = (e) => {
    e.preventDefault()
    
    if (validateField(this.state.username) && validateField(this.state.password)) {
      this.createUser()
    } else {
      this.setState ({ formError: 'Please correct fill in the fields'}) 
    }
  }

  handleReceiveToken = (e) => {
    e.preventDefault()
    
    if (validateField(this.state.username) && validateField(this.state.password)) {
      this.receiveToken()
    } else {
      this.setState ({ formError: 'Please correct fill in the fields'}) 
    }
  }

  handleOnChangePassword = (e) => {
    this.setState ({
      password: e.target.value
    })
  }

  handleOnChangeUsername = (e) => {
    this.setState ({
      username: e.target.value
    })
  }

  changeMode = () => {
    this.setState ({
      login: !this.state.login,
      formError: ''
    })
  }

  render () {
    return (
      <div className="form">
         <ul className="tab-group">
           <li onClick={this.changeMode} className={this.state.login ? 'tab active' : 'tab'}><a href="#login">Log In</a></li>
           <li onClick={this.changeMode} className={!this.state.login ? 'tab active' : 'tab'}><a href="#signup">Sign Up</a></li>
         </ul>
      
      <div className="tab-content">
        <Form  paramId={'login'}
               username={this.state.username}
               login={this.state.login}
               password={this.state.password}
               formError={this.state.formError}
               handleOnChangeUsername={this.handleOnChangeUsername}
               handleOnChangePassword={this.handleOnChangePassword}
               handleReceiveToken={this.handleReceiveToken}
        />

        <Form   paramId={'sign-up'}
                username={this.state.username}
                login={!this.state.login}
                password={this.state.password}
                formError={this.state.formError}
                handleOnChangePassword={this.handleOnChangePassword}
                handleOnChangeUsername={this.handleOnChangeUsername}
                handleCreateUserRequest={this.handleCreateUserRequest}
        />
      </div>
    </div>
    )
  }
}

Authentication.propTypes = {
  username: PropTypes.string,
  password: PropTypes.string,
  login: PropTypes.bool,
  formError: PropTypes.string
}