import React, { Component } from 'react' 
import axios from 'axios'
import PropTypes from 'prop-types'

import { generateId } from '../lib/utils'

import './Home.css'

export default class Home extends Component {
  constructor (props) {
    super(props)

    this.state = {
      users: []
    }
  }
  
  componentDidMount = () => {
    this.receiveUsers()
  }

  receiveUsers = () => {
    const url = 'https://grape-auth-boilerplate.herokuapp.com'

    axios.get(`${url}/users`)
      .then(response => this.setState ({ users: response.data }) )
      .catch(req => this.setState ({ formError: req.response.data.errors[0] }) )
  }

  render () {
    return (
      <div className="Home">
        <header className="home-header">
          <button className="signout-button"
                  onClick={this.props.handleRemoveToken}>SignOut
          </button>
        </header>
        <UserList users={this.state.users}/>
      </div>
    )
  }
}

const UserList = (props) => {
  const users = props.users.map((user) => {
    return <li className="user-item" key={generateId()}>{user.id} : {user.username}</li>
  })

  return (
    <ul className="user-list">
      {users}
    </ul>
  )
}

Home.propTypes = {
  handleRemoveToken: PropTypes.func
}