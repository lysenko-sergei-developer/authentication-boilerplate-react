import React from 'react'
import PropTypes from 'prop-types'

export const Form = (props) => {
  return (
    <div id={props.paramId} className={props.login ? 'display-block' : 'display-none'}>   
      <h1>{props.paramId === 'login' ? 'Welcome Back!' : 'Sign Up for Free'}</h1>
      <form>
        <div className="field-wrap">
          <input type="text" 
            placeholder="Username" 
            autoComplete="off"
            value={props.username}
            onChange={props.handleOnChangeUsername}/>
        </div>
          
        <div className="field-wrap">
          <input type="password" 
            placeholder="Password" 
            autoComplete="off" 
            value={props.password}
            onChange={props.handleOnChangePassword}/>
        </div>
        
        <span className="error">{props.formError}</span>

        <button className="button button-block"
                onClick={props.paramId === 'login' ? props.handleReceiveToken : props.handleCreateUserRequest}
        >
          {props.paramId === 'login' ? 'Log in' : 'Register now'}
        </button>
          
      </form>
    </div>
  )
} 

Form.propTypes = {
  paramId: PropTypes.string.isRequired,
  login: PropTypes.bool,
  username: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  handleOnChangeUsername: PropTypes.func,
  handleOnChangePassword: PropTypes.func,
  handleReceiveToken: PropTypes.func,
  handleCreateUserRequest: PropTypes.func
}